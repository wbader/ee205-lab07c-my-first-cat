///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab07c - My First Cat - EE 205 - Spr 2022
///
/// @file    hello2.cpp
/// @version 1.0 - Initial version
///
/// @author  Waylon Bader <wbader@hawaii.edu>
/// @date    3 Mar 2022
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

int main() 
{
   std::cout << "Hello World!" << std::endl;

   return 0;
}
