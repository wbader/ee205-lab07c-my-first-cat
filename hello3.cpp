///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab07c - My First Cat - EE 205 - Spr 2022
///
/// @file    hello3.cpp
/// @version 1.0 - Initial version
///
/// @author  Waylon Bader <wbader@hawaii.edu>
/// @date    3 Mar 2022
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

using namespace std;

class cat
{
public:
   void sayHello()
   {
       cout << "Meow!" << endl;
   }
};

int main() 
{
   cat myCat;

   myCat.sayHello();

   return 0; 
}
